// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "MoveBTTask.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALGAME_API UMoveBTTask : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	//contains logic for current task
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	//virtual void OnTaskFinished(UBehaviorTreeComponent& OwnerComp,
		//uint8* NodeMemory, EBTNodeResult::Type TaskResult);
};
