// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SurvivalGameProjectile.h"
#include "EnemyAI.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AR4Weapon.generated.h"

UCLASS()
class SURVIVALGAME_API AAR4Weapon : public AActor
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	AAR4Weapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* weapon;
	
	//fires projectiles
	void Fire();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float timePasses = 0.f;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class ASurvivalGameProjectile> ProjectileClass;

	UPROPERTY(BlueprintReadWrite, Category = Actor, meta = (AllowPrivateAccess = "true"))
		class ASurvivalGameCharacter* playerC;

	UPROPERTY(EditDefaultsOnly, Category = user)
		class AEnemyAI* enemy;
};
