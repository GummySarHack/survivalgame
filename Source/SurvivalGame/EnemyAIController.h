// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAI.h"
#include "AITargetPoint.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class SURVIVALGAME_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

private:

	//Behaviour Tree reference
	UBehaviorTreeComponent* bTC;

	//BB reference
	UBlackboardComponent* bBC;

	//BB key
	UPROPERTY(EditDefaultsOnly, Category = "BB Key")
		FName LocationToGo;

	//save my targets
	TArray<AActor*> targetPoints;

	virtual void OnPossess(APawn* pawn) override;

public:

	AEnemyAIController();

	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return bBC; }

	FORCEINLINE TArray<AActor*> GetAvailableTargetPoints() const { return targetPoints; }
};
