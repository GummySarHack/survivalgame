// Fill out your copyright notice in the Description page of Project Settings.


#include "KA74UWeapon.h"
#include "SurvivalGameCharacter.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Engine/Engine.h"



// Sets default values
AKA74UWeapon::AKA74UWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AKA74UWeapon::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AKA74UWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}



