// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveBTTask.h"
#include "EnemyAIController.h"
#include "AITargetPoint.h"
#include "EnemyAI.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/Engine.h"

EBTNodeResult::Type UMoveBTTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnemyAIController* enemyActor = Cast<AEnemyAIController>(OwnerComp.GetAIOwner());
	
	//check if valid
	if (enemyActor)
	{
		UBlackboardComponent* blackboardComp = enemyActor->GetBlackboardComp();
		AAITargetPoint* currentPoint = Cast<AAITargetPoint>(blackboardComp->GetValueAsObject("LocationToGo"));

		//get my targets
		TArray<AActor*> availableTargets = enemyActor->GetAvailableTargetPoints();

		//this variable will contain a random index in order to determine the next possible point
		int32 randomIndex;

		// Here,we store the possible next target point
		AAITargetPoint* nextTargetPoint = nullptr;

		//reference to controller to cast
		auto controller = Cast<AEnemyAIController>(this->GetClass());

		//Find a next point which is different from the current one

		do
		{
			randomIndex = FMath::RandRange(0, availableTargets.Num() - 1);
			//Remember that the array provided by the controller function contains AActor* objects, so we need to cast
			nextTargetPoint = Cast<AAITargetPoint>(availableTargets[randomIndex]);
		} while (currentPoint == nextTargetPoint);

		//Update the next location in the Blackboard so the bot can move tot he next Blackboard value
		blackboardComp->SetValueAsObject("LocationToGo", nextTargetPoint);

		//From there, it's a success so
		return EBTNodeResult::Succeeded;
	}
		return EBTNodeResult::Failed;
}

//void UMoveBTTask::OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult)
//{
//}
