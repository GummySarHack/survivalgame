// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AITargetPoint.h"

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/AIModule/Classes/Perception/PawnSensingComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "EnemyAI.generated.h"

class UInputComponent;

UCLASS()
class SURVIVALGAME_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

	
public:	
	// Sets default values for this actor's properties
	AEnemyAI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Patrol, meta = (AllowPrivateAccess = "true"))
		bool stopPatrol = false;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// *************** Behaviour tree variables *************** \\

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Target, meta = (AllowPrivateAccess = "true"))
		class AAITargetPoint* target;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Target, meta = (AllowPrivateAccess = "true"))
		class AAITargetPoint* target2;

	//BB reference
	UBlackboardComponent* blackBoardComp;
	//BHT reference
	UBehaviorTreeComponent* behaviourComp;

	//access BHT
	UPROPERTY(EditAnywhere, Category = "AI")
		class UBehaviorTree* behaviourTree;

	// *************** End ***************

	// *************** Animation bool variables *************** \\

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Animbools, meta = (AllowPrivateAccess = "true"))
		bool canRun = false;
	bool GetCanRun() { return canRun; };
	bool SetCanRun(bool newRun) { return canRun = newRun; };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Animbools, meta = (AllowPrivateAccess = "true"))
		bool canChase = false;
	bool GetCanChase() { return canChase; };
	bool SetCanChase(bool newChase) { return canChase = newChase; };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Animbools, meta = (AllowPrivateAccess = "true"))
		bool canShoot = false;
	bool GetCanShoot() { return canShoot; };
	bool SetCanShoot(bool newShot) { return canShoot = newShot; };

	bool GetStopPatrol() { return stopPatrol; };
	void SetStopPatrol(bool dontStop) { dontStop = stopPatrol; };

	// *************** Animation bool variables end *************** \\

	// *************** Sensoring *************** \\

	//On pawnSensing
	UFUNCTION(BlueprintCallable)
		void OnSeePawn(APawn* SeenPawn);
	
	UFUNCTION(BlueprintCallable)
		void OnHearNoise(APawn* heardPawn, const FVector& Location, float Volume);


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnSensingComponent* pawnSensor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
		UPawnNoiseEmitterComponent* hearSensor;

	// *************** Sensoring end *************** \\

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class ASurvivalGameProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimMontage* FireAnimation;
};
