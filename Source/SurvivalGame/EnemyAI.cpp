// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"

#include "SurvivalGameProjectile.h"
#include "AR4Weapon.h"
#include "EnemyAIController.h"
#include "SurvivalGameCharacter.h"

#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Perception/PawnSensingComponent.h"
#include "Engine/Engine.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h"
#include "MotionControllerComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Animation/AnimInstance.h"
#include "Math/Vector.h"

// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	blackBoardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackBoardComp"));
	behaviourComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviourComp"));

	pawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	pawnSensor->SensingInterval = .25f; // 4 times per second
	pawnSensor->bOnlySensePlayers = false;
	pawnSensor->bHearNoises = true;
	pawnSensor->SetPeripheralVisionAngle(75.f);
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();

	FScriptDelegate fScriptDelegate;
	fScriptDelegate.BindUFunction(this, "OnSeePawn");
	pawnSensor->OnSeePawn.AddUnique(fScriptDelegate);
	
	FScriptDelegate fScriptDelegateHear;
	fScriptDelegateHear.BindUFunction(this, "OnHearNosie");
	pawnSensor->OnHearNoise.AddUnique(fScriptDelegateHear);
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetStopPatrol();
}

void AEnemyAI::OnSeePawn(APawn* SeenPawn)
{
	auto stuff = Cast<AEnemyAIController>(GetController());
	auto playerPawn = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	
	FVector playerPos = playerPawn->GetActorLocation();
	float distance = (this->GetActorLocation() - playerPos).Size();

	//if is player
	if (SeenPawn == playerPawn)
	{
		if (distance < 1500.0f)
		{
			if (distance < 700.0f)
			{
				SetCanShoot(true);
				
				if (distance < 500.f)
				{
					SetCanChase(false);
				}
				else
				{
					SetCanChase(true);
				}
			}
			else
			{
				SetCanShoot(false);
			}
			stuff->GetBlackboardComp()->SetValueAsObject("Player", playerPawn);
			
			SetCanChase(true);
			SetStopPatrol(true);
		}
		else
		{
			//if (GEngine)
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("PawnSensingNOTfunction"));
			SetCanChase(false);
			stuff->GetBlackboardComp()->ClearValue("Player");
		}
	}
}


void AEnemyAI::OnHearNoise(APawn* heardPawn, const FVector& Location, float Volume)
{
	//auto bb = Cast<AEnemyAIController>(GetController());
	//auto playerPawn = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	//if (heardPawn == playerPawn)
	//{
	//	this->Event
	//}
}

//bool AEnemyAI::IsNoiseRelevant(const APawn& Pawn, const UPawnNoiseEmitterComponent& NoiseEmitterComponent, bool bSourceWithinNoiseEmitter) const
//{
//	return false;
//}
