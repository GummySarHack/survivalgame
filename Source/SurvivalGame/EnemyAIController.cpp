// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

AEnemyAIController::AEnemyAIController()
{
	bTC = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviourTreeComponent"));
	bBC = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComponent"));
	LocationToGo = "LocationToGo";
}

void AEnemyAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);

	//check if actor valid
	AEnemyAI* enemyActor = Cast<AEnemyAI>(pawn);

	targetPoints.Empty();
	targetPoints.Push(enemyActor->target);
	targetPoints.Push(enemyActor->target2);

	if (enemyActor)
	{
		//initialise blackboard
		if (enemyActor->behaviourTree->BlackboardAsset)
		{
			bBC->InitializeBlackboard(*(enemyActor->behaviourTree->BlackboardAsset));
		}
	}
	//set actor with behaviour tree
	bTC->StartTree(*enemyActor->behaviourTree);
}

