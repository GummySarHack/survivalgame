// Fill out your copyright notice in the Description page of Project Settings.

#include "AR4Weapon.h"
#include "SurvivalGameProjectile.h"
#include "SurvivalGameCharacter.h"
#include "EnemyAIController.h"

#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Pawn.h"
#include "Engine/Engine.h"



DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values
AAR4Weapon::AAR4Weapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	weapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun"));

	weapon->SetupAttachment(RootComponent);

	enemy = CreateDefaultSubobject<AEnemyAI>(TEXT("User"));
}

// Called when the game starts or when spawned
void AAR4Weapon::BeginPlay()
{
	Super::BeginPlay();

	playerC = Cast<ASurvivalGameCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void AAR4Weapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//
	timePasses++;
		
	if (timePasses >= 100.f)
	{
		Fire();
		timePasses = 0.f;
	}
}

void AAR4Weapon::Fire()
{
	if (ProjectileClass != NULL)
	{
		auto playerPawn = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

		FVector playerPos = playerPawn->GetActorLocation();
		float distance = (this->GetActorLocation() - playerPos).Size();

		auto playerRot = playerPawn->GetActorRotation();

		//if (enemy->canChase == true)
		if (distance < 700.0f)
		{
			FVector fireLocation = weapon->GetSocketLocation("Fire");

			//TODO -- change shooting direction/aim
			this->GetWorld()->SpawnActor<ASurvivalGameProjectile>(this->ProjectileClass, 
										fireLocation, 
										playerRot);
		}
	}
}

